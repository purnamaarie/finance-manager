/** @const {string} */
export const CREATE = "CREATE";

/** @const {string} */
export const UPDATE = "UPDATE";
