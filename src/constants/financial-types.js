/** @const {string} */
export const CREDIT = "CREDIT";

/** @const {string} */
export const DEBIT = "DEBIT";
