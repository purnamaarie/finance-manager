import { useEffect, useState } from "react";

export default () => {
  const [screen, setScreen] = useState();

  const update = () => setScreen(Boolean(window.innerWidth < 37.5 * 16));

  useEffect(() => {
    update();
    window.addEventListener("resize", update);

    return () => {
      window.removeEventListener("resize", update);
    };
  }, []);

  return screen;
};
