export default (history = null) => {
  const back = () => !!history && history.goBack();

  const push = (path, state = {}) =>
    !!history && window.location.pathname !== path && history.push(path, state);

  const replace = (path) => !!history && history.replace(path);

  return { back, push, replace };
};
