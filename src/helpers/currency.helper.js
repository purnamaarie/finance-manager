export default () => {
  const getCurrencyFormat = (value, showCurrency = true) => {
    const formatter = new Intl.NumberFormat("id-ID", {
      currency: "IDR",
      minimumFractionDigits: 0,
      style: "currency",
    });

    return formatter.format(value);
  };

  return { getCurrencyFormat };
};
