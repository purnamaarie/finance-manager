export const classNames = (...classNames) =>
  classNames.filter((i) => typeof i === "string" && !!i.trim()).join(" ");
