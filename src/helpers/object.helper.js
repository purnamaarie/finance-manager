export const updateObjKey = (obj, name, value) => ({
  ...obj,
  [name]: value,
});
