import { SETTING_TYPES } from "constants/action-types";
import { updateObjKey } from "helpers/object.helper";

const initialState = {
  financialTypes: [],
  hasLogin: false,
  transactions: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SETTING_TYPES.UPDATE_FINANCIAL_TYPES:
      return updateObjKey(state, "financialTypes", action.payload);
    case SETTING_TYPES.UPDATE_HAS_LOGIN:
      return updateObjKey(state, "hasLogin", action.payload);
    case SETTING_TYPES.UPDATE_TRANSACTIONS:
      return updateObjKey(state, "transactions", action.payload);
    default:
      return state;
  }
};
