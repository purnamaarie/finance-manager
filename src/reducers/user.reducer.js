import { USER_TYPES } from "constants/action-types";

const initialState = {
  birthdate: null,
  email: "",
  firstName: "",
  gender: 0,
  lastName: "",
};

export default (state = initialState, action) => {
  switch (action.type) {
    case USER_TYPES.UPDATE_PROFILE:
      return action.payload;
    default:
      return state;
  }
};
