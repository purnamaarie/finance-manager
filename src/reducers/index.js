import { combineReducers } from "redux";

import SettingReducer from "./setting.reducer";
import UserReducer from "./user.reducer";

export default combineReducers({
  setting: SettingReducer,
  user: UserReducer,
});
