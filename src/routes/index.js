import React, { Suspense } from "react";
import { Switch, Route } from "react-router-dom";

import Dashboard from "screens/Dashboard";
import Login from "screens/Login";
import Profile from "screens/Profile";
import Transactions from "screens/Transactions";
import FinancialTypes from "screens/FinancialTypes";

const routes = (
  <Suspense fallback={null}>
    <Switch>
      <Route exact path="/" component={Login} />
      <Route exact path="/dashboard" component={Dashboard} />
      <Route exact path="/profile" component={Profile} />
      <Route exact path="/transactions" component={Transactions} />
      <Route exact path="/financial-types" component={FinancialTypes} />
    </Switch>
  </Suspense>
);

export default routes;
