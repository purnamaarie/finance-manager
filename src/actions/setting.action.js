import { SETTING_TYPES } from "constants/action-types";

export const updateFinancialTypes = (financialTypes) => ({
  type: SETTING_TYPES.UPDATE_FINANCIAL_TYPES,
  payload: financialTypes,
});

export const updateHasLogin = (hasLogin) => ({
  type: SETTING_TYPES.UPDATE_HAS_LOGIN,
  payload: hasLogin,
});

export const updateTransactions = (transactions) => ({
  type: SETTING_TYPES.UPDATE_TRANSACTIONS,
  payload: transactions,
});
