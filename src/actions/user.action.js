import { USER_TYPES } from "constants/action-types";

export const updateProfile = (profile) => ({
  type: USER_TYPES.UPDATE_PROFILE,
  payload: profile,
});
