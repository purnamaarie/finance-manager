import React from "react";
import { connect } from "react-redux";
import { BrowserRouter } from "react-router-dom";
import styles from "./App.module.scss";

import { updateHasLogin } from "actions/setting.action";

import routes from "./routes";
import SettingContext from "contexts/setting.context";

const App = ({ firstName, hasLogin, lastName, updateHasLogin }) => (
  <BrowserRouter>
    <SettingContext.Provider
      value={{ firstName, hasLogin, lastName, updateHasLogin }}
    >
      <div className={styles.app}>{routes}</div>
    </SettingContext.Provider>
  </BrowserRouter>
);

const mapStateToProps = (state) => ({
  firstName: state.user.firstName,
  hasLogin: state.setting.hasLogin,
  lastName: state.user.lastName,
});

const mapDispatchToProps = (dispatch) => ({
  updateHasLogin: (payload) => dispatch(updateHasLogin(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
