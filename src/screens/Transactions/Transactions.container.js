import { connect } from "react-redux";
import Transactions from "./Transactions.component";
import { updateTransactions } from "actions/setting.action";

const mapStateToProps = (state) => ({
  financialTypes: state.setting.financialTypes,
  transactions: state.setting.transactions,
});

const mapDispatchToProps = (dispatch) => ({
  updateTransactions: (payload) => dispatch(updateTransactions(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Transactions);
