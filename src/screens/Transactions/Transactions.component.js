import React, { useCallback, useEffect, useMemo, useState } from "react";
import moment from "moment";
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Paper,
  Select,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
} from "@material-ui/core";
import { cloneDeep } from "lodash";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faClipboard,
  faMinus,
  faPencilAlt,
  faPlus,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import styles from "./Transactions.module.scss";

import { Header, Modal } from "components";
import { CREATE, UPDATE } from "constants/modal-types";
import { CREDIT, DEBIT } from "constants/financial-types";
import CurrencyHelper from "helpers/currency.helper";

const Transactions = ({
  financialTypes,
  history,
  transactions,
  updateTransactions,
}) => {
  const [state, setState] = useState({
    amount: 0,
    code: "",
    date: new Date(),
    description: "",
    financialType: "",
    modal: false,
    modalType: "",
    page: 0,
    rowsPerPage: 10,
    transactionList: transactions,
    type: DEBIT,
    search: "",
  });

  const { getCurrencyFormat } = CurrencyHelper();
  const {
    amount,
    code,
    date,
    description,
    financialType,
    modal,
    modalType,
    page,
    rowsPerPage,
    transactionList,
    type,
    search,
  } = state;

  useEffect(() => {
    updateTransactions(transactionList);
  }, [transactionList, updateTransactions]);

  const getFinancialName = useCallback(
    (code) => {
      const financialType = financialTypes.find((i) => i.code === code);

      return financialType.name;
    },
    [financialTypes]
  );

  const getList = useMemo(
    () =>
      transactionList
        .filter(
          (i) =>
            (i === "" ||
              i.code.toLowerCase().includes(search.toLowerCase()) ||
              i.type.toLowerCase().includes(search.toLowerCase()) ||
              i.amount.toLowerCase().includes(search.toLowerCase()) ||
              i.date.toLowerCase().includes(search.toLowerCase())) &&
            i
        )
        .slice(
          rowsPerPage === -1 ? 0 : page * rowsPerPage,
          rowsPerPage === -1 ? transactionList.length : (page + 1) * rowsPerPage
        ),
    [transactionList, page, rowsPerPage, search]
  );

  const handleCloseModal = () =>
    setState((prevState) => ({
      ...prevState,
      code: "",
      date: new Date(),
      description: "",
      financialType: "",
      modal: false,
      name: "",
      type: DEBIT,
    }));

  const handleOnChangePage = (_, page) =>
    setState((prevState) => ({ ...prevState, page }));

  const handleOnClearSearch = () =>
    setState((prevState) => ({ ...prevState, search: "" }));

  const handleOnDelete = (idx) =>
    window.confirm(
      `Are you sure want to delete ${transactionList[idx].name}?`
    ) &&
    setState((prevState) => {
      const transactionList = cloneDeep(prevState.transactionList);
      transactionList.splice(idx, 1);

      return { ...prevState, transactionList: transactionList };
    });

  const handleOnInputChange = (e, obj) => {
    e.persist();
    setState((prevState) => ({ ...prevState, [obj]: e.target.value }));
  };

  const handleOpenModal = (obj = null) =>
    setState((prevState) => ({
      ...prevState,
      modal: true,
      modalType: !!obj ? UPDATE : CREATE,
      ...obj,
    }));

  const handleOnRowsPerPageChange = (e) =>
    setState((prevState) => ({
      ...prevState,
      page: 0,
      rowsPerPage: e.target.value,
    }));

  const handleOnSubmit = (e) => {
    e.preventDefault();

    setState((prevState) => {
      const transactionList = cloneDeep(prevState.transactionList);
      const data = { amount, code, date, description, financialType, type };

      if (modalType === CREATE) transactionList.push(data);
      else {
        const idx = transactionList.findIndex((i) => i.code === code);
        transactionList[idx] = data;
      }

      return {
        ...prevState,
        amount: "",
        code: "",
        date: new Date(),
        description: "",
        financialType: "",
        modal: false,
        name: "",
        transactionList,
        type: DEBIT,
      };
    });
  };

  const renderModal = () => (
    <Modal open={modal} onClose={handleCloseModal}>
      <form className={styles.form} onSubmit={handleOnSubmit}>
        <h4>Transaction</h4>
        <TextField
          className={styles.input}
          label="Code"
          disabled={modalType === UPDATE}
          required
          tabIndex={1}
          value={code}
          onChange={(e) => handleOnInputChange(e, "code")}
        />
        <FormControl className={styles.input} tabIndex={2} required>
          <InputLabel id="financial-type">Financial Type</InputLabel>
          <Select
            labelId="financial-type"
            value={financialType}
            onChange={(e) => handleOnInputChange(e, "financialType")}
          >
            <MenuItem value="">- Please Select -</MenuItem>
            {financialTypes.map((i, idx) => (
              <MenuItem key={`financial-type-${idx}`} value={i.code}>
                {i.name}
              </MenuItem>
            ))}
          </Select>
        </FormControl>
        <TextField
          className={styles.input}
          type="date"
          label="Date"
          tabIndex={3}
          value={moment(date).format("YYYY-MM-DD")}
          onChange={(e) => handleOnInputChange(e, "date")}
        />
        <FormControl className={styles.input} tabIndex={4} required>
          <InputLabel id="type">Type</InputLabel>
          <Select
            labelId="type"
            value={type}
            onChange={(e) => handleOnInputChange(e, "type")}
          >
            <MenuItem value={CREDIT}>Credit</MenuItem>
            <MenuItem value={DEBIT}>Debit</MenuItem>
          </Select>
        </FormControl>
        <TextField
          className={styles.input}
          type="number"
          label="Amount"
          required
          tabIndex={5}
          value={amount}
          onChange={(e) => handleOnInputChange(e, "amount")}
        />
        <TextField
          className={styles.input}
          label="Description"
          multiline
          rows={4}
          tabIndex={6}
          value={description}
          onChange={(e) => handleOnInputChange(e, "description")}
        />
        <Button type="submit" color="primary" variant="contained" fullWidth>
          Save
        </Button>
      </form>
    </Modal>
  );

  return (
    <>
      <Header history={history} />
      <section className={styles.container}>
        <div className={styles.wrapper}>
          <div className={styles.header}>
            <div className={styles.headerTitleContainer}>
              <FontAwesomeIcon icon={faClipboard} />
              <span className={styles.title}>Transactions</span>
            </div>
            <div className={styles.headerWrapper}>
              <TextField
                className={styles.inputSearch}
                label="Search"
                placeholder="Search"
                value={search}
                InputProps={{
                  endAdornment: (
                    <FontAwesomeIcon
                      className={styles.timesIcon}
                      icon={faTimes}
                      onClick={handleOnClearSearch}
                    />
                  ),
                }}
                onChange={(e) => handleOnInputChange(e, "search")}
              />
              <Button
                className={styles.button}
                color="primary"
                startIcon={<FontAwesomeIcon icon={faPlus} />}
                onClick={() => handleOpenModal()}
              >
                Create
              </Button>
            </div>
          </div>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Code</TableCell>
                  <TableCell>Financial Type</TableCell>
                  <TableCell>Type</TableCell>
                  <TableCell>Amount</TableCell>
                  <TableCell>Date</TableCell>
                  <TableCell>Description</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {getList.length === 0 ? (
                  <TableRow>
                    <TableCell colSpan={7} align="center">
                      No Data
                    </TableCell>
                  </TableRow>
                ) : (
                  getList.map((i, idx) => (
                    <TableRow key={`row-${idx}`}>
                      <TableCell>{i.code}</TableCell>
                      <TableCell>{getFinancialName(i.financialType)}</TableCell>
                      <TableCell>{i.type}</TableCell>
                      <TableCell>{getCurrencyFormat(i.amount)}</TableCell>
                      <TableCell>
                        {moment(i.date).format("DD-MM-YYYY")}
                      </TableCell>
                      <TableCell>{i.description || "-"}</TableCell>
                      <TableCell>
                        <Button onClick={() => handleOpenModal(i)}>
                          <FontAwesomeIcon icon={faPencilAlt} />
                        </Button>
                        <Button onClick={() => handleOnDelete(idx)}>
                          <FontAwesomeIcon icon={faMinus} />
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))
                )}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[
                      10,
                      20,
                      40,
                      { label: "All", value: -1 },
                    ]}
                    colSpan={7}
                    count={getList.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleOnChangePage}
                    onChangeRowsPerPage={handleOnRowsPerPageChange}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
        </div>
      </section>
      {renderModal()}
    </>
  );
};

export default Transactions;
