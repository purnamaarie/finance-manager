import React, { useMemo } from "react";
import moment from "moment";
import { groupBy } from "lodash";
import { Grid } from "@material-ui/core";
import { Chart } from "react-google-charts";
import styles from "./Dashboard.module.scss";

import { Header } from "components";
import CurrencyHelper from "helpers/currency.helper";
import { DEBIT } from "constants/financial-types";

const Dashboard = ({ financialTypes, history, transactions }) => {
  const { getCurrencyFormat } = CurrencyHelper();

  const getCurrentBalance = useMemo(
    () =>
      transactions.reduce(
        (total, i) =>
          total + parseFloat(i.amount) * (i.type === DEBIT ? -1 : 1),
        0
      ),
    [transactions]
  );

  const getMonthlyExpenses = useMemo(() => {
    const grouping = transactions
      .filter(
        (i) =>
          i.type === DEBIT &&
          moment(i.date).isAfter(moment().subtract(31, "days"))
      )
      .reduce((acc, curr) => {
        if (!acc[curr.financialType]) acc[curr.financialType] = [];
        acc[curr.financialType].push(curr);

        return acc;
      }, []);

    const data = Object.keys(grouping).map((i) => {
      const financialType = financialTypes.find((j) => j.code === i);
      const amount = grouping[i].reduce(
        (total, j) => total + parseFloat(j.amount),
        0
      );

      return [financialType.name, amount];
    });

    return [["Type", "Amount"], ...data];
  }, [financialTypes, transactions]);

  const getMonthlyExpensesByFinancialType = useMemo(() => {
    const financialTypeNames = financialTypes.map((i) => i.name);

    const temp = groupBy(
      transactions.sort((a, b) => {
        const dateA = new Date(a.date);
        const dateB = new Date(b.date);

        return dateA - dateB;
      }),
      (i) => moment(i.date).startOf("month").format("MM-YYYY")
    );

    const result = [];
    Object.keys(temp).forEach((i) => {
      const group = groupBy(temp[i], (j) => j.financialType);

      const amount = Object.keys(group).map((j) => {
        const amount = group[j].reduce(
          (total, k) => total + parseFloat(k.amount),
          0
        );

        return { code: j, amount };
      });

      const temp2 = financialTypes.map((j) => {
        const data = amount.find((k) => k.code === j.code);

        return data ? data.amount : 0;
      });

      result.push([i, ...temp2]);
    });

    return [["Month", ...financialTypeNames], ...result];
  }, [financialTypes, transactions]);

  const renderLoader = () => <div>Loading Chart</div>;

  return (
    <>
      <Header history={history} />
      <section className={styles.container}>
        <div className={styles.wrapper}>
          <Grid container spacing={2}>
            <Grid className={styles.balanceContainer} item xs={12}>
              <h4>Current Balance:</h4>
              <h2 className={styles.balance}>
                {getCurrencyFormat(getCurrentBalance)}
              </h2>
            </Grid>
            <Grid item xs={12} sm={4}>
              <Chart
                height={240}
                chartType="PieChart"
                loader={renderLoader()}
                data={getMonthlyExpenses}
                options={{ title: "Expenses", is3D: true }}
              />
            </Grid>
            <Grid item xs={12} sm={8}>
              <Chart
                height={240}
                chartType="ComboChart"
                loader={renderLoader()}
                data={getMonthlyExpensesByFinancialType}
                options={{
                  title: "Monthly Expenses by Financial Type",
                  vAxis: { title: "Financial Types" },
                  hAxis: { title: "Month" },
                  seriesType: "bars",
                  series: { 5: { type: "line" } },
                }}
              />
            </Grid>
          </Grid>
        </div>
      </section>
    </>
  );
};

export default Dashboard;
