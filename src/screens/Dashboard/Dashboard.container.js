import { connect } from "react-redux";
import Dashboard from "./Dashboard.component";

const mapStateToProps = (state) => ({
  financialTypes: state.setting.financialTypes,
  transactions: state.setting.transactions,
});

export default connect(mapStateToProps)(Dashboard);
