import React, { useState, useEffect } from "react";
import { Button, TextField } from "@material-ui/core/";
import styles from "./Login.module.scss";

import RouterHelper from "helpers/router.helper";

const Login = ({ hasLogin, history, updateHasLogin, updateProfile }) => {
  const [state, setState] = useState({
    email: "",
    error: "",
    password: "",
  });

  const router = RouterHelper(history);
  const { email, error, password } = state;

  useEffect(() => {
    if (hasLogin) {
      router.replace("/dashboard");
      router.push("/dashboard");
    }
  }, [hasLogin, router]);

  const handleOnInputChange = (e, obj) => {
    e.persist();
    setState((prevState) => ({ ...prevState, [obj]: e.target.value }));
  };

  const handleOnSubmit = (e) => {
    e.preventDefault();
    setState((prevState) => ({ ...prevState, error: "" }));

    if (email !== "asd@asd.com" && password !== "asd123")
      setState((prevState) => ({
        ...prevState,
        error: "Wrong email/password",
      }));
    else {
      updateProfile({
        birthdate: new Date(),
        email,
        firstName: "Arie",
        gender: 1,
        lastName: "Purnama",
      });
      updateHasLogin(true);
    }
  };

  return (
    <section className={styles.container}>
      <form className={styles.wrapper} onSubmit={handleOnSubmit}>
        <h4>Personal Finance Manager</h4>
        <TextField
          className={styles.input}
          type="email"
          label="Email"
          value={email}
          required
          fullWidth
          onChange={(e) => handleOnInputChange(e, "email")}
        />
        <TextField
          className={styles.input}
          type="password"
          label="Password"
          value={password}
          required
          fullWidth
          onChange={(e) => handleOnInputChange(e, "password")}
        />
        {error && <div className={styles.errorText}>{error}</div>}
        <Button type="submit" color="primary" variant="contained" fullWidth>
          Login
        </Button>
      </form>
    </section>
  );
};

export default Login;
