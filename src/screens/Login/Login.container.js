import { connect } from "react-redux";
import Login from "./Login.component";

import { updateHasLogin } from "actions/setting.action";
import { updateProfile } from "actions/user.action";

const mapStateToProps = (state) => ({
  hasLogin: state.setting.hasLogin,
});

const mapDispatchToProps = (dispatch) => ({
  updateHasLogin: (payload) => dispatch(updateHasLogin(payload)),
  updateProfile: (payload) => dispatch(updateProfile(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
