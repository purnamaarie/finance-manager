import { connect } from "react-redux";
import Profile from "./Profile.component";

const mapStateToProps = (state) => ({
  birthdate: state.user.birthdate,
  email: state.user.email,
  firstName: state.user.firstName,
  gender: state.user.gender,
  lastName: state.user.lastName,
});

const mapDispatchToProps = (dispatch) => ({});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
