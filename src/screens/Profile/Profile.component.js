import React from "react";
import moment from "moment";
import {
  Button,
  FormControl,
  InputLabel,
  MenuItem,
  Select,
  TextField,
} from "@material-ui/core/";
import styles from "./Profile.module.scss";

import { Header } from "components";

const Profile = ({
  birthdate,
  email,
  firstName,
  gender,
  history,
  lastName,
}) => (
  <>
    <Header history={history} />
    <section className={styles.container}>
      <div className={styles.wrapper}>
        <h4>Profile</h4>
        <TextField
          className={styles.input}
          type="email"
          label="Email"
          value={email}
          disabled
        />
        <TextField
          className={styles.input}
          label="First Name"
          value={firstName}
          disabled
        />
        <TextField
          className={styles.input}
          label="Last Name"
          value={lastName}
          disabled
        />
        <TextField
          className={styles.input}
          type="date"
          label="Birthdate"
          disabled
          value={moment(birthdate).format("YYYY-MM-DD")}
        />
        <FormControl className={styles.input}>
          <InputLabel id="gender">Gender</InputLabel>
          <Select labelId="gender" value={gender} disabled>
            <MenuItem value={1}>Male</MenuItem>
            <MenuItem value={2}>Female</MenuItem>
          </Select>
        </FormControl>
        <Button
          className={styles.button}
          color="primary"
          disabled
          variant="contained"
        >
          Save
        </Button>
      </div>
    </section>
  </>
);

export default Profile;
