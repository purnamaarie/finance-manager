import { connect } from "react-redux";
import FinalcialTypes from "./FinancialTypes.component";
import { updateFinancialTypes } from "actions/setting.action";

const mapStateToProps = (state) => ({
  financialTypes: state.setting.financialTypes,
});

const mapDispatchToProps = (dispatch) => ({
  updateFinancialTypes: (payload) => dispatch(updateFinancialTypes(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(FinalcialTypes);
