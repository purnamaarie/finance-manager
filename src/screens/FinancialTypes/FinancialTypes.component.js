import React, { useEffect, useMemo, useState } from "react";
import {
  Button,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableFooter,
  TableHead,
  TablePagination,
  TableRow,
  TextField,
} from "@material-ui/core";
import { cloneDeep } from "lodash";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faDatabase,
  faMinus,
  faPencilAlt,
  faPlus,
  faTimes,
} from "@fortawesome/free-solid-svg-icons";
import styles from "./FinancialTypes.module.scss";

import { Header, Modal } from "components";
import { CREATE, UPDATE } from "constants/modal-types";

const FinancialTypes = ({ financialTypes, history, updateFinancialTypes }) => {
  const [state, setState] = useState({
    code: "",
    description: "",
    financialTypeList: financialTypes,
    modal: false,
    modalType: "",
    name: "",
    page: 0,
    rowsPerPage: 10,
    search: "",
  });

  const {
    code,
    description,
    financialTypeList,
    modal,
    modalType,
    name,
    page,
    rowsPerPage,
    search,
  } = state;

  useEffect(() => {
    updateFinancialTypes(financialTypeList);
  }, [financialTypeList, updateFinancialTypes]);

  const getList = useMemo(
    () =>
      financialTypeList
        .filter(
          (i) =>
            (i === "" ||
              i.code.toLowerCase().includes(search.toLowerCase()) ||
              i.name.toLowerCase().includes(search.toLowerCase())) &&
            i
        )
        .slice(
          rowsPerPage === -1 ? 0 : page * rowsPerPage,
          rowsPerPage === -1
            ? financialTypeList.length
            : (page + 1) * rowsPerPage
        ),
    [financialTypeList, page, rowsPerPage, search]
  );

  const handleCloseModal = () =>
    setState((prevState) => ({
      ...prevState,
      code: "",
      description: "",
      modal: false,
      name: "",
    }));

  const handleOnChangePage = (_, page) =>
    setState((prevState) => ({ ...prevState, page }));

  const handleOnClearSearch = () =>
    setState((prevState) => ({ ...prevState, search: "" }));

  const handleOnDelete = (idx) => {
    if (
      window.confirm(
        `Are you sure want to delete ${financialTypeList[idx].name}?`
      )
    ) {
      setState((prevState) => {
        const financialTypeList = cloneDeep(prevState.financialTypeList);
        financialTypeList.splice(idx, 1);

        return { ...prevState, financialTypeList };
      });
    }
  };

  const handleOnInputChange = (e, obj) => {
    e.persist();
    setState((prevState) => ({ ...prevState, [obj]: e.target.value }));
  };

  const handleOpenModal = (obj = null) =>
    setState((prevState) => ({
      ...prevState,
      modal: true,
      modalType: !!obj ? UPDATE : CREATE,
      ...obj,
    }));

  const handleOnRowsPerPageChange = (e) =>
    setState((prevState) => ({
      ...prevState,
      page: 0,
      rowsPerPage: e.target.value,
    }));

  const handleOnSubmit = (e) => {
    e.preventDefault();

    setState((prevState) => {
      const financialTypeList = cloneDeep(prevState.financialTypeList);
      const data = { code, name, description };

      if (modalType === CREATE) financialTypeList.push(data);
      else {
        const idx = financialTypeList.findIndex((i) => i.code === code);
        financialTypeList[idx] = data;
      }

      return {
        ...prevState,
        code: "",
        description: "",
        financialTypeList,
        modal: false,
        name: "",
      };
    });
  };

  const renderModal = () => (
    <Modal open={modal} onClose={handleCloseModal}>
      <form className={styles.form} onSubmit={handleOnSubmit}>
        <h4>Financial Type</h4>
        <TextField
          className={styles.input}
          label="Code"
          disabled={modalType === UPDATE}
          required
          tabIndex={1}
          value={code}
          onChange={(e) => handleOnInputChange(e, "code")}
        />
        <TextField
          className={styles.input}
          label="Name"
          required
          tabIndex={2}
          value={name}
          onChange={(e) => handleOnInputChange(e, "name")}
        />
        <TextField
          className={styles.input}
          label="Description"
          multiline
          rows={4}
          tabIndex={3}
          value={description}
          onChange={(e) => handleOnInputChange(e, "description")}
        />
        <Button type="submit" color="primary" variant="contained" fullWidth>
          Save
        </Button>
      </form>
    </Modal>
  );

  return (
    <>
      <Header history={history} />
      <section className={styles.container}>
        <div className={styles.wrapper}>
          <div className={styles.header}>
            <div className={styles.headerTitleContainer}>
              <FontAwesomeIcon icon={faDatabase} />
              <span className={styles.title}>Financial Types</span>
            </div>
            <div className={styles.headerWrapper}>
              <TextField
                className={styles.inputSearch}
                label="Search"
                placeholder="Search"
                value={search}
                InputProps={{
                  endAdornment: (
                    <FontAwesomeIcon
                      className={styles.timesIcon}
                      icon={faTimes}
                      onClick={handleOnClearSearch}
                    />
                  ),
                }}
                onChange={(e) => handleOnInputChange(e, "search")}
              />
              <Button
                className={styles.button}
                color="primary"
                startIcon={<FontAwesomeIcon icon={faPlus} />}
                onClick={() => handleOpenModal()}
              >
                Create
              </Button>
            </div>
          </div>
          <TableContainer component={Paper}>
            <Table aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell>Code</TableCell>
                  <TableCell>Name</TableCell>
                  <TableCell>Description</TableCell>
                  <TableCell>Actions</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {getList.length === 0 ? (
                  <TableRow>
                    <TableCell colSpan={4} align="center">
                      No Data
                    </TableCell>
                  </TableRow>
                ) : (
                  getList.map((i, idx) => (
                    <TableRow key={`row-${idx}`}>
                      <TableCell>{i.code}</TableCell>
                      <TableCell>{i.name}</TableCell>
                      <TableCell>{i.description || "-"}</TableCell>
                      <TableCell>
                        <Button onClick={() => handleOpenModal(i)}>
                          <FontAwesomeIcon icon={faPencilAlt} />
                        </Button>
                        <Button onClick={() => handleOnDelete(idx)}>
                          <FontAwesomeIcon icon={faMinus} />
                        </Button>
                      </TableCell>
                    </TableRow>
                  ))
                )}
              </TableBody>
              <TableFooter>
                <TableRow>
                  <TablePagination
                    rowsPerPageOptions={[
                      10,
                      20,
                      40,
                      { label: "All", value: -1 },
                    ]}
                    colSpan={4}
                    count={getList.length}
                    rowsPerPage={rowsPerPage}
                    page={page}
                    onChangePage={handleOnChangePage}
                    onChangeRowsPerPage={handleOnRowsPerPageChange}
                  />
                </TableRow>
              </TableFooter>
            </Table>
          </TableContainer>
        </div>
      </section>
      {renderModal()}
    </>
  );
};

export default FinancialTypes;
