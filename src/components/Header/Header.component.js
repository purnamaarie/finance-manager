import React, { memo, useContext, useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import {
  Button,
  Divider,
  Drawer,
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
  Menu,
  MenuItem,
} from "@material-ui/core";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faBars,
  faCaretDown,
  faCaretUp,
  faClipboard,
  faDatabase,
  faHome,
  faSignOutAlt,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import PropTypes from "prop-types";
import styles from "./Header.module.scss";

import SettingContext from "contexts/setting.context";
import RouterHelper from "helpers/router.helper";
import MobileDetectHelper from "helpers/mobileDetect.helper";

const Header = ({ history }) => {
  const [state, setState] = useState({
    drawer: false,
    dropdown: null,
  });

  const router = RouterHelper(history);
  const isMobile = MobileDetectHelper();
  const { firstName, hasLogin, lastName, updateHasLogin } = useContext(
    SettingContext
  );
  const { drawer, dropdown } = state;

  useEffect(() => {
    if (hasLogin !== undefined && !hasLogin) {
      router.replace("/");
      router.push("/");
    }
  }, [hasLogin, router]);

  const handleCloseDropdown = () =>
    setState((prevState) => ({ ...prevState, dropdown: null }));

  const handleOnLogout = () => updateHasLogin(false);

  const handleOpenDropdown = (e) =>
    setState((prevState) => ({ ...prevState, dropdown: e.currentTarget }));

  const handleToggleDrawer = () =>
    setState((prevState) => ({ ...prevState, drawer: !prevState.drawer }));

  const renderDrawer = () => (
    <Drawer anchor="left" open={drawer} onClose={handleToggleDrawer}>
      <List>
        <NavLink className={styles.link} to="/dashboard">
          <ListItem button>
            <ListItemIcon>
              <FontAwesomeIcon icon={faHome} />
            </ListItemIcon>
            <ListItemText primary="Dashboard" />
          </ListItem>
        </NavLink>
        <NavLink className={styles.link} to="/financial-types">
          <ListItem button>
            <ListItemIcon>
              <FontAwesomeIcon icon={faDatabase} />
            </ListItemIcon>
            <ListItemText primary="Financial Types" />
          </ListItem>
        </NavLink>
        <NavLink className={styles.link} to="/transactions">
          <ListItem button>
            <ListItemIcon>
              <FontAwesomeIcon icon={faClipboard} />
            </ListItemIcon>
            <ListItemText primary="Transactions" />
          </ListItem>
        </NavLink>
      </List>
    </Drawer>
  );

  const renderMenu = () => (
    <Menu
      anchorEl={dropdown}
      keepMounted
      open={Boolean(dropdown)}
      onClose={handleCloseDropdown}
    >
      <MenuItem>
        <NavLink className={styles.link} to="/profile">
          <FontAwesomeIcon className={styles.menuItemIcon} icon={faUser} />
          Profile
        </NavLink>
      </MenuItem>
      <Divider />
      <MenuItem onClick={handleOnLogout}>
        <FontAwesomeIcon className={styles.menuItemIcon} icon={faSignOutAlt} />{" "}
        Logout
      </MenuItem>
    </Menu>
  );

  return (
    <>
      <header className={styles.container}>
        <Button
          startIcon={<FontAwesomeIcon icon={faBars} />}
          onClick={handleToggleDrawer}
        >
          {!isMobile && `Personal Finance Manager`}
        </Button>
        <Button
          aria-controls="simple-menu"
          aria-haspopup="true"
          endIcon={
            <FontAwesomeIcon icon={dropdown ? faCaretUp : faCaretDown} />
          }
          onClick={handleOpenDropdown}
        >
          {`${firstName} ${lastName}`}
        </Button>
      </header>
      {renderDrawer()}
      {renderMenu()}
    </>
  );
};

Header.propTypes = {
  history: PropTypes.object.isRequired,
};

export default memo(Header);
