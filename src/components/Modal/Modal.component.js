import React, { memo } from "react";
import { Fade, Modal as MaterialModlal } from "@material-ui/core";
import styles from "./Modal.module.scss";

const Modal = ({ children, open, ...tempProps }) => (
  <MaterialModlal {...tempProps} className={styles.modal} open={open}>
    <Fade in={open}>
      <div className={styles.container}>{children}</div>
    </Fade>
  </MaterialModlal>
);

export default memo(Modal);
